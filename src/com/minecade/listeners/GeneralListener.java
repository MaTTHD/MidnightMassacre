package com.minecade.listeners;

import com.minecade.MidnightMassacre;
import com.minecade.Team;
import com.minecade.TeamHandler;
import com.minecade.game.GameHandler;
import com.minecade.game.GameState;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.MetadataValue;

import java.util.Arrays;

/**
 * Created by Matt on 19/10/2015.
 */
public class GeneralListener implements Listener {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        if(plugin.getState() == GameState.Lobby){
            plugin.setupLobbyPlayer(e.getPlayer());
            KitManager.getInstance().chooseKit(e.getPlayer(), KitManager.getInstance().getAllKits().get(0));
            TeamHandler.getInstance().addPlayerToTeam(e.getPlayer(), TeamHandler.getInstance().getTeamWithLess());
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        if(plugin.getState() != GameState.Ingame){
            return;
        }
        Player player = e.getPlayer();
        Kit kit = KitManager.getInstance().getKitForPlayer(player);
        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
            kit.callRightClick(player, null);
        }
        return;
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player && e.getEntity() instanceof Player){
            Player damager = (Player) e.getDamager();
            Player victim = (Player) e.getEntity();

            if(GameHandler.getInstance().getSpectators().contains(damager.getName()) || GameHandler.getInstance().getSpectators().contains(victim.getName())){
                e.setCancelled(true);
                return;
            }
            Kit kit = KitManager.getInstance().getKitForPlayer(damager);
            kit.callLeftClick(damager, victim);
        }

        else if(e.getDamager() instanceof Player && e.getEntity() instanceof IronGolem){
            Player damager = (Player) e.getDamager();
            IronGolem golem = (IronGolem) e.getEntity();
            if(GameHandler.getInstance().getSpectators().contains(damager.getName())){
                e.setCancelled(true);
                return;
            }
            Kit kit = KitManager.getInstance().getKitForPlayer(damager);
            kit.callLeftClick(damager, golem);
        }
        else if(e.getDamager() instanceof Arrow && e.getEntity() instanceof Player){
            Arrow arrow = (Arrow) e.getEntity();
            Player victim = (Player) e.getEntity();

            if(GameHandler.getInstance().getSpectators().contains(victim.getName())){
                e.setCancelled(true);
                return;
            }

            if(arrow.hasMetadata("goblinarrow")){
                victim.damage(1.5);
            }
            else {
                return;
            }
        }
        else if(e.getDamager() instanceof Arrow && e.getEntity() instanceof IronGolem){
            Arrow arrow = (Arrow) e.getEntity();
            IronGolem victim = (IronGolem) e.getEntity();

            if(arrow.hasMetadata("goblinarrow")){
                victim.damage(15);
            }
            else {
                return;
            }
        }
        return;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){

    }

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(e.getEntity() instanceof Player){
            Player player = (Player) e.getEntity();
            if(GameHandler.getInstance().getSpectators().contains(player.getName())){
                e.setCancelled(true);
                return;
            }
            Team team = TeamHandler.getInstance().getTeamForPlayer(player);

            EntityDamageEvent.DamageCause cause = e.getCause();
            double damage = e.getDamage();
            if((player.getHealth() - e.getDamage()) <=0){
                e.setCancelled(true);
                GameHandler.getInstance().respawn(player);
            }
        }
        else if(e.getEntity() instanceof IronGolem){
            GameHandler.getInstance().stop(TeamHandler.getInstance().getTeamByName("attacker"));
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onLobbyInteract(PlayerInteractEvent e){
        if(plugin.getState() == GameState.Lobby){
            if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
                if(e.getItem().getType() == Material.PAPER){
                    if(e.getItem().getItemMeta().getDisplayName().contains("Class Selector")){
                        e.setCancelled(true);
                        e.getPlayer().openInventory(plugin.getKitSelector());
                    }
                    else if(e.getItem().getItemMeta().getDisplayName().contains("Team Selector")){
                        e.setCancelled(true);
                        e.getPlayer().openInventory(plugin.getTeamSelector());
                    }
                }
            }
        }
        else {
            return;
        }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        Player pl = (Player) e.getWhoClicked();
        if(e.getInventory().getTitle().contains("Class Selector")){
            e.setCancelled(true);
            ItemStack item = e.getCurrentItem();
            if(item == null){
                return;
            }
            Kit chosen = KitManager.getInstance().getKit(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
            KitManager.getInstance().chooseKit(pl, chosen);
            return;
        }
        else if(e.getInventory().getTitle().contains("Team Selector")){
            e.setCancelled(true);
            ItemStack item = e.getCurrentItem();
            if(item == null){
                return;
            }
            Team chosen = TeamHandler.getInstance().getTeamByName(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
            TeamHandler.getInstance().addPlayerToTeam(pl, chosen);
            return;
        }
    }
}
