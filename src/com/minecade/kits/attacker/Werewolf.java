package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Created by Matt on 16/10/2015.
 */
public class Werewolf extends Kit {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta swordMeta = sword.getItemMeta();
        swordMeta.setDisplayName(plugin.getText("kit.werewolf.sword.name"));
        sword.setItemMeta(swordMeta);
        sword.addEnchantment(Enchantment.KNOCKBACK, 1);
        //SWORD

        ItemStack eggs = new ItemStack(Material.getMaterial(383), 3, (short) 95);
        ItemMeta eggMeta = eggs.getItemMeta();
        eggMeta.setDisplayName(plugin.getText("kit.werewolf.egg.name"));
        return new ItemStack[] {sword, eggs};
    }

    @Override
    public ItemStack[] getArmor() {

        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET,1);
        LeatherArmorMeta helmMeta = (LeatherArmorMeta) helmet.getItemMeta();
        helmMeta.setColor(Color.BLACK);

        ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE,1);
        LeatherArmorMeta chestMeta = (LeatherArmorMeta) chestPlate.getItemMeta();
        chestMeta.setColor(Color.BLACK);
        chestPlate.setItemMeta(chestMeta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS,1);
        LeatherArmorMeta legMeta = (LeatherArmorMeta) leggings.getItemMeta();
        legMeta.setColor(Color.BLACK);
        leggings.setItemMeta(legMeta);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS,1);
        LeatherArmorMeta bootMeta = (LeatherArmorMeta) boots.getItemMeta();
        bootMeta.setColor(Color.BLACK);
        boots.setItemMeta(bootMeta);

        return new ItemStack[]{helmet, chestPlate, leggings, boots};
    }

    @Override
    public int getMaxHealth() {
        return 7;
    }

    @Override
    public String getName() {
        return "Werewolf";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.getMaterial(383),1,(short) 95);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(plugin.getText("kit.werewolf.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {

    }

    @Override
    public void callLeftClick(Player player, Entity victim) {

    }
}
