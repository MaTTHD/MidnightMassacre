package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Matt on 17/10/2015.
 */
public class Skeleton extends Kit {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @Override
    public ItemStack[] getItems() {
        ItemStack bow = new ItemStack(Material.BOW,1);
        ItemStack arrow = new ItemStack(Material.ARROW, 1);
        bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        return new ItemStack[]{bow,arrow};
    }

    @Override
    public ItemStack[] getArmor() {
        return new ItemStack[] {};
    }

    @Override
    public int getMaxHealth() {
        return 7;
    }

    @Override
    public String getName() {
        return "Skeleton";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.getMaterial(397), 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.skeleton.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {

    }

    @Override
    public void callLeftClick(Player player, Entity victim) {

    }
}
