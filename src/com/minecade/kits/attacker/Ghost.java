package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 17/10/2015.
 */
public class Ghost extends Kit {
    @Override
    public ItemStack[] getItems() {
        ItemStack feather = new ItemStack(Material.FEATHER, 1);
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
        return new ItemStack[]{sword, feather};
    }

    @Override
    public ItemStack[] getArmor() {
        ItemStack helmet = new ItemStack(Material.PUMPKIN,1);

        ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE,1,(short) 8);
        LeatherArmorMeta chestMeta = (LeatherArmorMeta) chestPlate.getItemMeta();
        chestMeta.setColor(Color.WHITE);
        chestPlate.setItemMeta(chestMeta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS,1,(short) 15);
        LeatherArmorMeta legMeta = (LeatherArmorMeta) leggings.getItemMeta();
        legMeta.setColor(Color.WHITE);
        leggings.setItemMeta(legMeta);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS,1,(short) 8);
        LeatherArmorMeta bootMeta = (LeatherArmorMeta) boots.getItemMeta();
        bootMeta.setColor(Color.WHITE);
        boots.setItemMeta(bootMeta);

        return new ItemStack[]{helmet, chestPlate, leggings, boots};
    }

    @Override
    public int getMaxHealth() {
        return 7;
    }

    @Override
    public String getName() {
        return "Ghost";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.FEATHER, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.ghost.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {
        ItemStack item = player.getItemInHand();
        if(item != null){
            if(item.getType() == Material.FEATHER){
                if(KitManager.getInstance().getFlying().contains(player.getName())){
                    player.sendMessage(MidnightMassacre.getPlugin().getText("kit.ghost.fly.disabled"));
                    return;
                }
                else {
                    player.setAllowFlight(true);
                    player.setFlying(true);
                    new BukkitRunnable(){
                        public void run(){
                            KitManager.getInstance().getFlying().remove(player.getName());
                        }
                    }.runTaskLater(MidnightMassacre.getPlugin(), 45*20);
                    new BukkitRunnable(){
                        public void run(){
                            player.setAllowFlight(false);
                            player.setFlying(false);
                        }
                    }.runTaskLater(MidnightMassacre.getPlugin(), 10);
                }
            }
        }
    }

    @Override
    public void callLeftClick(Player player, Entity victim) {

    }
}
