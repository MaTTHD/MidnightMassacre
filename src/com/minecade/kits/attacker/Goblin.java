package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 17/10/2015.
 */
public class Goblin extends Kit {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);

        return new ItemStack[] {sword};
    }

    @Override
    public ItemStack[] getArmor() {
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET,1);
        LeatherArmorMeta helmMeta = (LeatherArmorMeta) helmet.getItemMeta();
        helmMeta.setColor(Color.GREEN);

        ItemStack chestPlate = new ItemStack(Material.LEATHER_CHESTPLATE,1);
        LeatherArmorMeta chestMeta = (LeatherArmorMeta) chestPlate.getItemMeta();
        chestMeta.setColor(Color.GREEN);
        chestPlate.setItemMeta(chestMeta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS,1);
        LeatherArmorMeta legMeta = (LeatherArmorMeta) leggings.getItemMeta();
        legMeta.setColor(Color.GREEN);
        leggings.setItemMeta(legMeta);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS,1);
        LeatherArmorMeta bootMeta = (LeatherArmorMeta) boots.getItemMeta();
        bootMeta.setColor(Color.GREEN);
        boots.setItemMeta(bootMeta);

        return new ItemStack[]{helmet, chestPlate, leggings, boots};
    }

    @Override
    public int getMaxHealth() {
        return 7;
    }

    @Override
    public String getName() {
        return "Goblin";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 2);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.goblin.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {
        ItemStack item = player.getItemInHand();
        if(item == null){
            return;
        }
        if(item.getType() == Material.STONE_SWORD){
            if(KitManager.getInstance().getArrow().contains(player.getName())){
                player.sendMessage(MidnightMassacre.getPlugin().getText("kit.goblin.arrow"));
                return;
            }
            else {
                Arrow arrow = player.launchProjectile(Arrow.class);
                arrow.setMetadata("goblinarrow", new FixedMetadataValue(MidnightMassacre.getPlugin(), "arrow"));
                KitManager.getInstance().getArrow().add(player.getName());
                new BukkitRunnable(){
                    public void run(){
                        KitManager.getInstance().getArrow().remove(player.getName());
                    }
                }.runTaskLater(MidnightMassacre.getPlugin(), 20*20);
            }
        }
    }

    @Override
    public void callLeftClick(Player player, Entity victim) {

    }

}
