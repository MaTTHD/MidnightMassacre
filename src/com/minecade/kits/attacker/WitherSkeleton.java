package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 17/10/2015.
 */
public class WitherSkeleton extends Kit {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
        return new ItemStack[]{sword};
    }

    @Override
    public ItemStack[] getArmor() {
        return new ItemStack[]{};
    }

    @Override
    public int getMaxHealth() {
        return 7;
    }

    @Override
    public String getName() {
        return "Wither Skeleton";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.getMaterial(397), 1, (short) 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.witherskeleton.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {
        ItemStack item = player.getItemInHand();
        if (item == null) {
            return;
        }

        if (item.getType() != Material.WOOD_SWORD) {
            return;
        }

        if(KitManager.getInstance().getWither().contains(player.getName())){
            return;
        }

        player.launchProjectile(WitherSkull.class).setVelocity(player.getEyeLocation().getDirection().multiply(2));
        KitManager.getInstance().getWither().add(player.getName());
        new BukkitRunnable(){
            public void run(){
                KitManager.getInstance().getWither().remove(player.getName());
            }
        }.runTaskLater(MidnightMassacre.getPlugin(), 20*20L);
    }

    @Override
    public void callLeftClick(Player player, Entity victim) {

    }
}
