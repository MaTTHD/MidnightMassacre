package com.minecade.kits.attacker;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Matt on 17/10/2015.
 */
public class Vampire extends Kit {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
        ItemMeta meta = sword.getItemMeta();
        meta.getLore().add(plugin.getText("kit.vampire.sword.lore"));
        sword.setItemMeta(meta);
        return new ItemStack[]{sword};
    }

    @Override
    public ItemStack[] getArmor() {
        return new ItemStack[]{};
    }

    @Override
    public int getMaxHealth() {
        return 5;
    }

    @Override
    public String getName() {
        return "Vampire";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.getMaterial(383), 1, (short) 65);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.vampire.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {

    }

    @Override
    public void callLeftClick(Player player, Entity victim) {
        if (victim instanceof Player) {
            ItemStack item = player.getItemInHand();
            if(item != null){
                if(item.getType() == Material.WOOD_SWORD){
                    Bat bat = (Bat) player.getWorld().spawnEntity(player.getLocation(), EntityType.BAT);
                    bat.setVelocity(player.getEyeLocation().getDirection().multiply(3));
                }
            }
        }
    }
}
