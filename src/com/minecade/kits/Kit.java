package com.minecade.kits;


import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Matt on 16/10/2015.
 */
public abstract class Kit {

    public abstract ItemStack[] getItems();
    public abstract ItemStack[] getArmor();
    public abstract int getMaxHealth();
    public abstract String getName();
    public abstract ItemStack getDisplayItem();
    public abstract void callRightClick(Player player, Entity victim);
    public abstract void callLeftClick(Player player, Entity victim);
}
