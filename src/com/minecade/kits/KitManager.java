package com.minecade.kits;

import com.minecade.MidnightMassacre;
import org.bukkit.entity.Flying;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 16/10/2015.
 */
public class KitManager {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();
    private List<Kit> allKits = new ArrayList<>();
    private Map<String, Kit> playerKits = new HashMap<>();

    private List<String> flying = new ArrayList<>();
    private List<String> arrow = new ArrayList<>();
    private List<String> wither = new ArrayList<>();

    public Kit getKit(String name) {
        for (Kit kit : allKits) {
            if (kit.getName().equalsIgnoreCase(name)) {
                return kit;
            }
        }
        return null;
    }

    public List<String> getFlying() {
        return flying;
    }

    public List<String> getWither() {
        return wither;
    }

    public List<String> getArrow() {
        return arrow;
    }

    private static KitManager instance;

    public static KitManager getInstance() {
        if (instance == null) {
            instance = new KitManager();
        }
        return instance;
    }

    public List<Kit> getAllKits() {
        return allKits;
    }

    public void chooseKit(Player player, Kit kit) {
        if (hasKit(player)) {
            playerKits.remove(player.getName());
        }
        playerKits.put(player.getName(), kit);
        player.sendMessage(plugin.getText("kit.choose.message", kit.getName()));
    }

    public void removeFromKit(Player player) {
        if (playerKits.containsKey(player.getName())) {
            playerKits.remove(player);
            player.sendMessage(plugin.getText("kit.remove.message"));
        }
    }

    public boolean hasKit(Player player) {
        if (playerKits.containsKey(player.getName())) {
            return true;
        } else {
            return false;
        }
    }

    public Kit getKitForPlayer(Player player) {
        return playerKits.get(player.getName());
    }


}
