package com.minecade.kits.defender;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

/**
 * Created by Matt on 19/10/2015.
 */
public class Zombie extends Kit {

    private Random ran = new Random();

    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        return new ItemStack[]{sword};
    }

    @Override
    public ItemStack[] getArmor() {
        ItemStack helmet = new ItemStack(Material.IRON_HELMET, 1);

        ItemStack chestPlate = new ItemStack(Material.IRON_CHESTPLATE, 1);

        ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS, 1);

        ItemStack boots = new ItemStack(Material.IRON_BOOTS, 1);

        return new ItemStack[]{helmet, chestPlate, leggings, boots};
    }

    @Override
    public int getMaxHealth() {
        return 15;
    }

    @Override
    public String getName() {
        return "Zombie";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 2);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.zombie.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {
        return;
    }

    @Override
    public void callLeftClick(Player player, Entity victim) {
        ItemStack item = player.getItemInHand();
        if (item == null) {
            return;
        }
        if (item.getType() == Material.STONE_SWORD) {
            if (victim == null) {
                return;
            }
            float chance = ran.nextFloat();
            if (victim instanceof Player) {
                if (chance <= 0.25) {
                    ((Player) victim).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 5, 3));
                    victim.sendMessage(MidnightMassacre.getPlugin().getText("zombie.bite.message"));
                    return;
                } else if (chance <= 0.10) {
                    ((Player) victim).setFoodLevel(3);
                    victim.sendMessage(MidnightMassacre.getPlugin().getText("zombie.bite.message"));
                    return;
                } else {
                    return;
                }
            }
            return;
        }

    }
}
