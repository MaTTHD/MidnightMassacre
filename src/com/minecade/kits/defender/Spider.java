package com.minecade.kits.defender;

import com.minecade.MidnightMassacre;
import com.minecade.kits.Kit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * Created by Matt on 18/10/2015.
 */
public class Spider extends Kit {

    Random ran = new Random();


    @Override
    public ItemStack[] getItems() {
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
        ItemStack rod = new ItemStack(Material.FISHING_ROD, 1);
        return new ItemStack[]{sword, rod};
    }

    @Override
    public ItemStack[] getArmor() {
        ItemStack helmet = new ItemStack(Material.CHAINMAIL_HELMET, 1);

        ItemStack chestPlate = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);

        ItemStack leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS, 1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS, 1);

        return new ItemStack[]{helmet, chestPlate, leggings, boots};
    }


    @Override
    public int getMaxHealth() {
        return 15;
    }

    @Override
    public String getName() {
        return "Spider";
    }

    @Override
    public ItemStack getDisplayItem() {
        ItemStack item = new ItemStack(Material.WEB, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(MidnightMassacre.getPlugin().getText("kit.spider.name"));
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void callRightClick(Player player, Entity victim) {
        return;
    }

    @Override
    public void callLeftClick(Player player, Entity victim) {
        ItemStack item = player.getItemInHand();
        if (item == null) {
            return;
        }
        if (item.getType() == Material.IRON_SWORD) {
            float chance = ran.nextFloat();
            if (chance <= 0.25) {
                if (victim != null) {
                    if (victim instanceof Player) {
                        ((Player)victim).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 3 * 20, 1));
                    }
                    else {
                        return;
                    }
                }
                return;
            }
        }
        return;
    }
}
