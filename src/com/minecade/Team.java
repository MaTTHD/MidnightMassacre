package com.minecade;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 16/10/2015.
 */
public class Team {

    private String name;
    private int maxSize = 8;
    private List<String> members;

    public Team(String name) {
        this.name = name;
        this.members = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
