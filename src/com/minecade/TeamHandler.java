package com.minecade;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 16/10/2015.
 */
public class TeamHandler {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    private List<Team> allTeams = new ArrayList<>();
    int teamSize = 8;

    private static TeamHandler instance;

    public static TeamHandler getInstance(){
        if(instance == null){
            instance = new TeamHandler();
        }
        return instance;
    }

    public Team getTeamByName(String name){
        for(Team t : allTeams){
            if(t.getName().equalsIgnoreCase(name)){
                return t;
            }
        }
        return null;
    }

    public void addPlayerToTeam(Player player, Team team){
        if(getPlayersAliveForTeam(team) >= 8){
            player.sendMessage(plugin.getText("team.is.full.message", team.getName()));
            return;
        }
        else {
            if(getTeamForPlayer(player) == null){
                team.getMembers().add(player.getName());
                player.sendMessage(plugin.getText("team.join.message", team.getName()));
            }
            else {
                player.sendMessage(plugin.getText("team.change.message", getTeamForPlayer(player).getName(), team.getName()));
                getTeamForPlayer(player).getMembers().remove(player.getName());
                team.getMembers().add(player.getName());
            }
        }
    }

    public void removePlayerFromTeam(Player player){
        Team current = getTeamForPlayer(player);
        if(current == null){
            player.sendMessage(plugin.getText("team.noteam.message"));
        }
        else {
            player.sendMessage(plugin.getText("team.leave.message", current.getName()));
            current.getMembers().remove(player.getName());
        }
    }

    public int getPlayersAliveForTeam(Team teamToFind){
        return teamToFind.getMembers().size();
    }

    public List<String> getAttackers() {
        for(Team t : allTeams){
            if(t.getName().equalsIgnoreCase("attacker")){
                return t.getMembers();
            }
        }
        return null;
    }

    public List<String> getDefenders() {
        for(Team t : allTeams){
            if(t.getName().equalsIgnoreCase("defender")){
                return t.getMembers();
            }
        }
        return null;
    }

    public List<Team> getAllTeams() {
        return allTeams;
    }

    public Team getTeamForPlayer(Player player){
        for(Team t : allTeams){
            if(t.getMembers().contains(player.getName())){
                return t;
            }
        }
        return null;
    }

    public Team getTeamWithLess(){
        Team attacker = getTeamByName("attacker");
        Team defender = getTeamByName("defender");
        if(attacker.getMembers().size() >= defender.getMembers().size()){
            return defender;
        }
        else {
            return attacker;
        }
    }
}
