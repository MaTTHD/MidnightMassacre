package com.minecade;

import com.minecade.commands.CommandManager;
import com.minecade.game.GameHandler;
import com.minecade.game.GameState;
import com.minecade.game.GameTimer;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import com.minecade.kits.attacker.*;
import com.minecade.kits.defender.Spider;
import com.minecade.kits.defender.Zombie;
import com.minecade.listeners.GeneralListener;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by Matt on 13/10/2015.
 */
public class MidnightMassacre extends JavaPlugin {

    private static MidnightMassacre plugin;

    private int ticks;
    private GameState state;
    ResourceBundle resourceBundle;

    private int lobbyTime;
    private int gameTime;

    private Map<String, Integer> playerLivesLeft = new HashMap<>();
    public void onEnable() {
        plugin = this;

        getConfig().options().copyDefaults(true);
        saveConfig();



        setupTranslations();

        CommandManager cm = new CommandManager();
        getCommand("mm").setExecutor(cm);

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new GeneralListener(), this);

        lobbyTime = getConfig().getInt("lobbyTime", 60);
        gameTime = getConfig().getInt("gameTime", 60 * 10);

        ticks = lobbyTime;
        state = GameState.Lobby;

        createTeams();
        loadKits();
        GameHandler.getInstance().load();

        new GameTimer().runTaskTimer(this, 0L, 20L);
    }

    public void onDisable() {

    }

    public void loadKits(){
        KitManager.getInstance().getAllKits().add(new Ghost());
        KitManager.getInstance().getAllKits().add(new Goblin());
        KitManager.getInstance().getAllKits().add(new Skeleton());
        KitManager.getInstance().getAllKits().add(new Vampire());
        KitManager.getInstance().getAllKits().add(new Werewolf());
        KitManager.getInstance().getAllKits().add(new WitherSkeleton());
        KitManager.getInstance().getAllKits().add(new Spider());
        KitManager.getInstance().getAllKits().add(new Zombie());
    }

    public void createTeams(){
        Team att = new Team("attacker");
        Team def = new Team("defender");
        TeamHandler.getInstance().getAllTeams().add(att);
        TeamHandler.getInstance().getAllTeams().add(def);
    }

    public static MidnightMassacre getPlugin() {
        return plugin;
    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public void setupTranslations() {
        String locale = getConfig().getString("locale", "en");
        resourceBundle = ResourceBundle.getBundle("text", new Locale(locale));
        // i18n messages
        getLogger().info(String.format("Plugin locale is set as: %s.", locale));
    }

    public String getText(String key, String... args) {
        if (resourceBundle != null && resourceBundle.containsKey(key)) {
            String result = ChatColor.translateAlternateColorCodes('&', resourceBundle.getString(key));
            for (int i = 0; i < args.length; i++) {
                result = result.replace("%" + (i + 1) + "%", args[i]);
            }
            return result;
        }

        return key;
    }

    public int getLobbyTime() {
        return lobbyTime;
    }

    public void setLobbyTime(int lobbyTime) {
        this.lobbyTime = lobbyTime;
    }

    public int getGameTime() {
        return gameTime;
    }

    public void setGameTime(int gameTime) {
        this.gameTime = gameTime;
    }

    public void setupLobbyPlayer(Player player) {
        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(getText("kit.selector.name"));
            item.setItemMeta(meta);
            player.getInventory().setItem(4, item);
        }

        {
            ItemStack item = new ItemStack(Material.PAPER, 1);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(getText("team.selector.name"));
            item.setItemMeta(meta);
            player.getInventory().setItem(0, item);
        }
        player.getInventory().setHeldItemSlot(0);
    }

    public Inventory getKitSelector(){
        Inventory inv = Bukkit.createInventory(null, 9, "Class Selector");
        for(Kit kit : KitManager.getInstance().getAllKits()){
            inv.addItem(kit.getDisplayItem());
        }
        return inv;
    }

    public Inventory getTeamSelector(){
        Inventory inv = Bukkit.createInventory(null, 9, "Team Selector");
        {
            ItemStack item = new ItemStack(Material.WOOL, 1, (short) 14);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(getText("team.attacker.name"));
            item.setItemMeta(meta);
            inv.setItem(3, item);
        }
        {
            ItemStack item = new ItemStack(Material.WOOL, 1, (short) 11);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(getText("team.defender.name"));
            item.setItemMeta(meta);
            inv.setItem(4, item);
        }
        return inv;
    }

    public String serializeLocation(Location loc){
        return loc.getWorld().getName().trim() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    public Location deserializeLocation(String loc){
        String[] split = loc.split(",");
        World world = Bukkit.getWorld(split[0]);
        int x = Integer.valueOf(split[1]);
        int y = Integer.valueOf(split[2]);
        int z = Integer.valueOf(split[3]);
        float yaw = Float.valueOf(split[4]);
        float pitch = Float.valueOf(split[5]);
        return new Location(world, x,y,z,yaw,pitch);
    }

    public Map<String, Integer> getPlayerLivesLeft() {
        return playerLivesLeft;
    }
}
