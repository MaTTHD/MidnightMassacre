package com.minecade.game;

import com.minecade.MidnightMassacre;
import com.minecade.TeamHandler;
import com.minecade.title.Title;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 13/10/2015.
 */
public class GameTimer extends BukkitRunnable {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();


    @Override
    public void run() {
        if (Bukkit.getOnlinePlayers().size() < 2) {
            plugin.setTicks(plugin.getLobbyTime());
            return;
        }
        if (plugin.getTicks() > 0) {
            plugin.setTicks(plugin.getTicks() - 1);
        }

        Title inGameTitle = new Title(plugin.getText("game.title.message"), plugin.getText("game.subtitle.message", String.valueOf(plugin.getTicks())), 0, 1, 0);
        Title title = new Title(plugin.getText("lobby.title.message"), plugin.getText("lobby.subtitle.message", String.valueOf(plugin.getTicks())), 0, 1, 0);

        if (plugin.getState() == GameState.Lobby) {
            switch (plugin.getTicks()) {
                case 30:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 20:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 10:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 5:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 4:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 3:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 2:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 1:
                    Bukkit.getOnlinePlayers().forEach(title::send);
                    break;
                case 0:
                    if (GameHandler.getInstance().getCurrentArena().isReady()) {
                        GameHandler.getInstance().start();
                    } else {
                        Bukkit.broadcastMessage(plugin.getText("game.not.ready.message"));
                        plugin.setTicks(plugin.getLobbyTime());
                    }
            }
        } else if (plugin.getState() == GameState.Ingame) {
            switch (plugin.getTicks()) {
                case 30:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 20:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 10:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 5:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 4:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 3:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 2:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 1:
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        title.send(pl);
                        pl.getWorld().playSound(pl.getLocation(), Sound.NOTE_STICKS, 1, 1);
                    }
                    break;
                case 0:
                    GameHandler.getInstance().stop(TeamHandler.getInstance().getTeamByName("defender"));
            }
        }
    }
}
