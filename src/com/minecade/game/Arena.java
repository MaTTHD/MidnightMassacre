package com.minecade.game;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.minecade.MidnightMassacre;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Matt on 13/10/2015.
 */
public class Arena {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();
    private String name;
    private boolean isCurrentArena;
    private Location golemSpawn, attackerSpawn, defenderSpawn;

    public Arena(String name){
        this.name = name;
        this.golemSpawn = plugin.deserializeLocation(plugin.getConfig().getString("maps." + name + ".golemSpawn"));
        this.attackerSpawn = plugin.deserializeLocation(plugin.getConfig().getString("maps." + name + ".attackerSpawn"));
        this.defenderSpawn = plugin.deserializeLocation(plugin.getConfig().getString("maps." + name + ".defenderSpawn"));
    }

    public Arena(String name, boolean isCurrentArena, Location golemSpawn, Location attackerSpawn, Location defenderSpawn) {
        this.name = name;
        this.isCurrentArena = isCurrentArena;
        this.golemSpawn = golemSpawn;
        this.attackerSpawn = attackerSpawn;
        this.defenderSpawn = defenderSpawn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCurrentArena() {
        return isCurrentArena;
    }

    public void setIsCurrentArena(boolean isCurrentArena) {
        this.isCurrentArena = isCurrentArena;
    }

    public Location getGolemSpawn() {
        return golemSpawn;
    }

    public void setGolemSpawn(Location golemSpawn) {
        this.golemSpawn = golemSpawn;
    }

    public Location getAttackerSpawn() {
        return attackerSpawn;
    }

    public void setAttackerSpawn(Location attackerSpawn) {
        this.attackerSpawn = attackerSpawn;
    }

    public Location getDefenderSpawn() {
        return defenderSpawn;
    }

    public void setDefenderSpawn(Location defenderSpawn) {
        this.defenderSpawn = defenderSpawn;
    }

    public boolean isReady(){
        return golemSpawn != null && attackerSpawn != null && defenderSpawn != null;
    }

    public void setupArena(){
        if(golemSpawn == null){
            System.out.print("SPAWN");
        }

        if(golemSpawn.getWorld() == null){
            System.out.println("WORLD IS NULL");
        }
        IronGolem golem = (IronGolem) golemSpawn.getWorld().spawnEntity(golemSpawn, EntityType.IRON_GOLEM);
        golem.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, Integer.MAX_VALUE));
        golem.setMaxHealth(100);
        golem.setHealth(100);
        Hologram holo = HologramsAPI.createHologram(plugin, golem.getEyeLocation().add(0,1,0));
        holo.insertTextLine(0, plugin.getText("golem.holo.name"));
        holo.insertTextLine(1, plugin.getText("golem.holo.health", String.valueOf(golem.getHealth())));
    }
}
