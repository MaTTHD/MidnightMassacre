package com.minecade.game;

/**
 * Created by Matt on 13/10/2015.
 */
public enum GameState {
    Lobby, Ingame, Offline;
}
