package com.minecade.game;

import com.minecade.MidnightMassacre;
import com.minecade.Team;
import com.minecade.TeamHandler;
import com.minecade.kits.Kit;
import com.minecade.kits.KitManager;
import com.minecade.title.Title;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Matt on 13/10/2015.
 */
public class GameHandler {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    private List<Arena> arenas = new ArrayList<>();
    private Arena currentArena;
    private TeamHandler teamHandler = TeamHandler.getInstance();
    private Map<String, Integer> deaths = new HashMap<>();
    private List<String> spectators = new ArrayList<>();

    public void removePlayer(Player player) {
        TeamHandler.getInstance().removePlayerFromTeam(player);
        KitManager.getInstance().getFlying().remove(player.getName());
        KitManager.getInstance().getWither().remove(player.getName());
        KitManager.getInstance().getArrow().remove(player.getName());

    }

    public List<String> getSpectators() {
        return spectators;
    }

    public Map<String, Integer> getDeaths() {
        return deaths;
    }

    private static GameHandler instance;

    public static GameHandler getInstance() {
        if (instance == null) {
            instance = new GameHandler();
        }
        return instance;
    }

    public void load() {
        if(plugin.getConfig().getConfigurationSection("maps") == null){
            return;
        }
        for (String map : plugin.getConfig().getConfigurationSection("maps").getKeys(false)) {
            Arena a = new Arena(map);
            arenas.add(a);
            System.out.println("Added " + a.getName() + " to maps!");
        }
        Random random = new Random();
        currentArena = arenas.get(0);
    }

    public Arena getArenaByName(String name) {
        for (Arena a : arenas) {
            if (a.getName().equalsIgnoreCase(name)) {
                return a;
            }
        }
        return null;
    }

    public Arena getCurrentArena() {
        return currentArena;
    }

    public void setCurrentArena(Arena a) {
        this.currentArena = a;
    }

    public List<Arena> getArenas() {
        return arenas;
    }

    public void start() {
        plugin.setState(GameState.Ingame);
        plugin.setTicks(plugin.getGameTime());
        currentArena.setupArena();
        for (Team t : TeamHandler.getInstance().getAllTeams()) {
            for (String s : t.getMembers()) {
                Player pl = Bukkit.getPlayer(s);
                Location location = getLocationForTeam(pl);
                pl.teleport(location);
                Kit kit = KitManager.getInstance().getKitForPlayer(pl);
                for (ItemStack item : kit.getItems()) {
                    pl.getInventory().addItem(item);
                }
                if(kit.getArmor().length == 4){
                    pl.getInventory().setHelmet(kit.getArmor()[0]);
                    pl.getInventory().setChestplate(kit.getArmor()[1]);
                    pl.getInventory().setLeggings(kit.getArmor()[2]);
                    pl.getInventory().setBoots(kit.getArmor()[3]);
                }
            }
        }
        Bukkit.broadcastMessage(plugin.getText("game.start.message"));
    }

    public Location getLocationForTeam(Player pl) {
        if (teamHandler.getTeamForPlayer(pl).getName().equalsIgnoreCase("attacker")) {
            return currentArena.getAttackerSpawn();
        } else {
            return currentArena.getDefenderSpawn();
        }
    }

    public void respawn(Player player) {
        Team team = TeamHandler.getInstance().getTeamForPlayer(player);
        if (team.getName().equalsIgnoreCase("attackers")) {
            if (deaths.containsKey(player.getName())) {
                if (deaths.get(player.getName()) >= 3) {
                    deaths.remove(player.getName());
                    spectators.add(player.getName());
                    player.sendMessage(plugin.getText("player.spectator"));
                    return;
                } else {
                    deaths.put(player.getName(), deaths.get(player.getName()) + 1);
                }
            } else {
                deaths.put(player.getName(), 1);
            }
        } else {
            spectators.add(player.getName());
            team.getMembers().remove(player.getName());
            player.setHealth(10);
            for (Player pl : Bukkit.getOnlinePlayers()) {
                if (spectators.contains(pl.getName())) {
                    continue;
                }
                pl.hidePlayer(pl);
            }
            player.sendMessage(plugin.getText("player.spectator"));
            return;
        }

        Kit kit = KitManager.getInstance().getKitForPlayer(player);
        player.getInventory().clear();
        player.setMaxHealth(kit.getMaxHealth());
        player.setHealth(kit.getMaxHealth());
        for (ItemStack item : kit.getItems()) {
            player.getInventory().addItem(item);
        }
        player.getInventory().setHelmet(kit.getArmor()[0]);
        player.getInventory().setChestplate(kit.getArmor()[1]);
        player.getInventory().setLeggings(kit.getArmor()[2]);
        player.getInventory().setBoots(kit.getArmor()[3]);
        if (kit.getName().contains("Zombie")) {
            player.setFoodLevel(3);
        }
        if (kit.getName().contains("Vampire")) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
        }
        if (kit.getName().contains("Skeleton")) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));
        }
        player.teleport(getLocationForTeam(player));
    }

    public void stop(Team team) {
        plugin.setState(GameState.Lobby);
        plugin.setTicks(plugin.getLobbyTime());
        if (team.getName().equalsIgnoreCase("attackers")) {
            Title winTitle = new Title(plugin.getText("attacker.win.title"), plugin.getText("attacker.win.subtitle"), 0, 1, 0);
            for (String s : teamHandler.getAttackers()) {
                Player pl = Bukkit.getPlayer(s);
                winTitle.send(pl);
                //TODO give participation coins using core!
            }
        } else {

            Title loseTitle = new Title(plugin.getText("attacker.lose.title"), plugin.getText("attacker.lose.subtitle"), 0, 1, 0);
            for (String s : teamHandler.getAttackers()) {
                Player pl = Bukkit.getPlayer(s);
                loseTitle.send(pl);
                //TODO give participation coins using core!
            }

            Title winTitle = new Title(plugin.getText("defender.win.title"), plugin.getText("defender.win.subtitle"), 0, 1, 0);
            for (String s : teamHandler.getDefenders()) {
                Player pl = Bukkit.getPlayer(s);
                winTitle.send(pl);
                //TODO give coins using core!
            }
        }
    }

    public void forceStop() {
        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.sendMessage(plugin.getText("game.force.stop.message"));
        }

        new BukkitRunnable() {
            public void run() {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.kickPlayer("Game over");
                }
            }
        }.runTaskLater(plugin, 5 * 20);
    }
}
