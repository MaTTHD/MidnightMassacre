package com.minecade.commands;

import com.minecade.MidnightMassacre;
import com.minecade.game.Arena;
import com.minecade.game.GameHandler;
import com.minecade.game.GameState;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 16/10/2015.
 */
public class CommandManager implements CommandExecutor {

    private MidnightMassacre plugin = MidnightMassacre.getPlugin();
    private List<String> settingUpPlayers = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("mm")) {
            if (!(sender instanceof Player)) {
                return true;
            }

            Player player = (Player) sender;

            if(args.length == 0){
                if(player.hasPermission("mm.setup")){
                    player.sendMessage(plugin.getText("setup.help.create"));
                    player.sendMessage(plugin.getText("setup.help.delete"));
                    player.sendMessage(plugin.getText("setup.help.position"));
                    return true;
                }
                return true;
            }
            else if(args.length == 2){
                if(args[0].equalsIgnoreCase("create")){
                    String name = args[1];
                    if(GameHandler.getInstance().getArenaByName(name) != null){
                        player.sendMessage(plugin.getText("arena.exists.message", name));
                        return true;
                    }
                    else {
                        Arena arena = new Arena(name, GameHandler.getInstance().getCurrentArena() == null, player.getLocation(), player.getLocation(), player.getLocation());
                        GameHandler.getInstance().getArenas().add(arena);
                        plugin.getConfig().set("maps." + name + ".golemSpawn", plugin.serializeLocation(arena.getGolemSpawn()));
                        plugin.getConfig().set("maps." + name + ".attackerSpawn", plugin.serializeLocation(arena.getAttackerSpawn()));
                        plugin.getConfig().set("maps." + name + ".defenderSpawn", plugin.serializeLocation(arena.getDefenderSpawn()));
                        plugin.saveConfig();

                        player.sendMessage(plugin.getText("arena.created.message", name));
                        player.sendMessage(plugin.getText("arena.setspawns.reminder"));
                        return true;
                    }
                }
                else if(args[0].equalsIgnoreCase("delete")){
                    String name = args[1];
                    if(GameHandler.getInstance().getArenaByName(name) == null){
                        player.sendMessage(plugin.getText("arena.not.found", name));
                        return true;
                    }
                    else {
                        if(GameHandler.getInstance().getCurrentArena().getName().equalsIgnoreCase(name) && plugin.getState() == GameState.Ingame){
                            player.sendMessage(plugin.getText("arena.in.use.message", name));
                            return true;
                        }
                        else {
                            plugin.getConfig().getConfigurationSection("maps").set(name, null);
                            player.sendMessage(plugin.getText("arena.deleted.message", name));
                            GameHandler.getInstance().getArenas().remove(GameHandler.getInstance().getArenaByName(name));
                            return true;
                        }
                    }
                }
                else {
                    return true;
                }
            }
            else if(args.length == 3){
                if(args[0].equalsIgnoreCase("set")){
                    String name = args[1];
                    Arena arena = GameHandler.getInstance().getArenaByName(name);
                    if(arena == null){
                        player.sendMessage(plugin.getText("arena.not.found", name));
                        return true;
                    }
                    else {
                        if(args[2].equalsIgnoreCase("golem")){
                            arena.setGolemSpawn(player.getLocation());
                            plugin.getConfig().set("maps." + name + ".golemSpawn", plugin.serializeLocation(player.getLocation()));
                            plugin.saveConfig();
                            return true;
                        }
                        else if(args[2].equalsIgnoreCase("attacker")){
                            arena.setAttackerSpawn(player.getLocation());
                            plugin.getConfig().set("maps." + name + ".attackerSpawn", plugin.serializeLocation(player.getLocation()));
                            plugin.saveConfig();
                            return true;
                        }
                        else if(args[2].equalsIgnoreCase("defender")){
                            arena.setDefenderSpawn(player.getLocation());
                            plugin.getConfig().set("maps." + name + ".defenderSpawn", plugin.serializeLocation(player.getLocation()));
                            plugin.saveConfig();
                            return true;
                        }
                    }
                }
            }
            else {
                return true;
            }
        }
        return false;
    }
}
