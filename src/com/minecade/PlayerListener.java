package com.minecade;

import com.minecade.game.GameHandler;
import com.minecade.game.GameState;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Matt on 16/10/2015.
 */
public class PlayerListener implements Listener {
    private MidnightMassacre plugin = MidnightMassacre.getPlugin();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){

    }

    @EventHandler
    public void onLeave(PlayerJoinEvent e){
        Player pl = e.getPlayer();
        TeamHandler.getInstance().removePlayerFromTeam(pl);
        if(canPlay()){
            return;
        }
        else {
            GameHandler.getInstance().forceStop();
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerInteract(PlayerInteractEvent e){
        if(plugin.getState() == GameState.Lobby){
            if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
                if(e.getItem().getType() == Material.PAPER){
                    if(e.getItem().getItemMeta().getDisplayName().contains("Class Selector")){
                        e.setCancelled(true);
                        e.getPlayer().openInventory(plugin.getKitSelector());
                    }
                    else if(e.getItem().getItemMeta().getDisplayName().contains("Team Selector")){
                        e.setCancelled(true);
                        e.getPlayer().openInventory(plugin.getTeamSelector());
                    }
                }
            }
        }
        else {
            return;
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Player){
            Player pl = (Player) e.getEntity();
            Team team = TeamHandler.getInstance().getTeamForPlayer(pl);

            if((pl.getHealth() - e.getDamage()) > 0){
                return;
            }

            if(e.getDamager() instanceof Player){
                Player damager = (Player)e.getDamager();
                //Player killed a player.


            }
        }
    }

    public boolean canPlay(){
        if(TeamHandler.getInstance().getAttackers().size() > 0 && TeamHandler.getInstance().getDefenders().size() > 0){
            return true;
        }
        else {
            return false;
        }
    }
}
